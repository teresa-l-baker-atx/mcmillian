package com.mcmillian.webservice.service;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mcmillian.webservice.service.exception.InvalidInputException;
import com.mcmillian.webservice.service.exception.MovieApiException;
import com.mcmillian.webservice.service.exception.MovieNotFoundException;

public class TestMoviesApi {

//	void getMovie(@PathParam("id") String id) throws InvalidInputException, MovieNotFoundException, MovieApiException;
//	void getNowPlaying() throws MovieApiException;

	
	@Test(expected=InvalidInputException.class)
	public void testGetMovieNullId() {
		throw new InvalidInputException("invalid id");
	}
	
	@Test(expected=MovieNotFoundException.class)
	public void testGetMovieNotFound() {
		throw new MovieNotFoundException("movie not found");
	}
	
	@Test(expected=MovieApiException.class)
	public void testGetMovieErrorReturned() {
		throw new MovieApiException("error returned");
	}
	
	@Test
	public void testGetMovieConnectionIssue() {
//		try {
//			throw new ClientProtocolException("http connection issue");
//		} catch (ClientProtocolException e) {
			assertTrue( true );
//		}
//		fail();
	}
	
	@Test
	public void testGetMovieSuccess() {
		assertTrue( true );
	}
	
	
	@Test
	public void testGetNowPlayingConnectionIssue() {
		// would throw ClientProtocolException
		assertTrue( true );
	}
	
	@Test(expected=MovieApiException.class)
	public void testGetNowPlayingErrorReturned() {
		throw new MovieApiException("error returned");
	}
	
	@Test
	public void testGetNowPlayingSuccess() {
		assertTrue( true );
	}
	
}
