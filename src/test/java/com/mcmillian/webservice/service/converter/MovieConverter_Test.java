package com.mcmillian.webservice.service.converter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mcmillian.entity.MovieEntity;

/**
 * Not a very useful test since MovieConverter not working as it. Need to mock MovieEntry response
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MovieConverter_Test {

	private MovieConverter movieConverter;
	
	@Before
	public void setUp() throws Exception {
		movieConverter = new MovieConverter();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testConvertToMovie() {
		String responseText = "test data not in json format";
		MovieEntity movie = movieConverter.convertToMovie(responseText);
		assertNotNull(movie);
	}
	
	@Test
	public void convertToMovieList() {
		String responseText = "test data not in json format";
		List<MovieEntity> movieList = movieConverter.convertToMovieList(responseText);
		assertNotNull(movieList);
		assertTrue(movieList.size() == 0);
	}
}
