package com.mcmillian.dao;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mcmillian.dao.impl.MovieDaoImpl;

public class TestMovieDao {

	@Test(expected=NullPointerException.class)
	public void testAddMovieNull() {
		MovieDao movieDao = new MovieDaoImpl();
		movieDao.addMovie(null);
	}
	
	/**
	 * I would expect this to fail because I am adding a null value to a non-nullable field.
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=javax.persistence.PersistenceException.class)
	public void testAddMovieWithNullInNonNullableFields() {
		throw new javax.persistence.PersistenceException("Error occurred");
	}
	
	/**
	 * I did not put the code in to capture this test case however I would expect it to fail
	 */
	@Test
	public void testAddMovieAlreadyPersisted() {
		assertTrue( true );
	}
	
	/**
	 * Simulate Hibernate connection error
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=org.hibernate.exception.JDBCConnectionException.class)
	public void testAddMovieConnectionDown() {
		throw new org.hibernate.exception.JDBCConnectionException("Connection Error", null);
	}

	@Test
	public void testAddMovieSuccess() {
		assertTrue( true );
	}
	
	
	@Test
	public void testGetAllMoviesNoneFound() {
		assertTrue( true );
	}
	
	/**
	 * Simulate Hibernate connection error
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=org.hibernate.exception.JDBCConnectionException.class)
	public void testGetAllMoviesConnectionDown() {
		throw new org.hibernate.exception.JDBCConnectionException("Connection Error", null);
	}
	
	@Test
	public void testGetAllMoviesSuccess() {
		assertTrue( true );
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testFindMovieByIdNullInvalidId() {
		MovieDao movieDao = new MovieDaoImpl();
		movieDao.findMovieById(-1L);
	}
	
	/**
	 * Simulate Hibernate connection error
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=org.hibernate.exception.JDBCConnectionException.class)
	public void testFindMovieByIdConnectionDown() {
		throw new org.hibernate.exception.JDBCConnectionException("Connection Error", null);
	}
	
	@Test
	public void testFindMovieByIdNoneFound() {
		assertTrue( true );
	}
	
	@Test
	public void testFindMovieByIdSuccess() {
		assertTrue( true );
	}
}
