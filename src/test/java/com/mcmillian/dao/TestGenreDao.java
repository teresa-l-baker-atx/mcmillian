package com.mcmillian.dao;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mcmillian.dao.impl.GenreDaoImpl;

public class TestGenreDao {

	@Test(expected=NullPointerException.class)
	public void testAddGenreNull() {
		GenreDao genreDao = new GenreDaoImpl();
		genreDao.addGenre(null);
	}
	
	/**
	 * I would expect this to fail because I am adding a null value to a non-nullable field.
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=javax.persistence.PersistenceException.class)
	public void testAddGenreNullInNonNullableFields() {
		throw new javax.persistence.PersistenceException("Error occurred");
	}
	
	/**
	 * I did not put the code in to capture this test case however I would expect it to fail
	 */
	@Test
	public void testAddGenreAlreadyPersisted() {
		assertTrue( true );
	}
	 
	/**
	 * Simulate Hibernate connection error
	 * Took best guess at exception that would be thrown
	 */
	@Test(expected=org.hibernate.exception.JDBCConnectionException.class)
	public void testAddGenreConnectionDown() {
		throw new org.hibernate.exception.JDBCConnectionException("Connection Error", null);
	}

	@Test
	public void testAddGenreSuccess() {
		assertTrue( true );
	}
	
}
