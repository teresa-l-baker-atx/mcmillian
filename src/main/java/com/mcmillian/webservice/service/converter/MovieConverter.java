package com.mcmillian.webservice.service.converter;

import java.util.ArrayList;
import java.util.List;

import com.mcmillian.entity.MovieEntity;

public class MovieConverter {

	public MovieEntity convertToMovie(String responseText) {
		MovieEntity movie = new MovieEntity();
		
		// convert to json, pick fields out and populate movie object.
		// This will include parsing the genre ids.
		// This might include creating a separate ws call to get name associated with genre id... Depends on a number of things...
		
		return movie;
	}
	
	public List<MovieEntity> convertToMovieList(String responseText) {
		List<MovieEntity> movieList = new ArrayList<MovieEntity>();
		
		// convert to json, pick fields out and populate movie objects. Will be an array of result objects
		// This will include parsing the genre ids
		
		return movieList;
	}
}
