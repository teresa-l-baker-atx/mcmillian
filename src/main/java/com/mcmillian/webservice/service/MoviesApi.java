package com.mcmillian.webservice.service;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.http.client.ClientProtocolException;

import com.mcmillian.webservice.service.exception.InvalidInputException;
import com.mcmillian.webservice.service.exception.MovieApiException;
import com.mcmillian.webservice.service.exception.MovieNotFoundException;

@Path("/movies")
public interface MoviesApi {
	
	/**
	 * @param id - id of movie being searched for
	 * @return Movie
	 * @throws InvalidInputException - thrown if input id is invalid
	 * @throws MovieNotFoundException - thrown if movie related to the id is not found
	 * @throws MovieApiException - thrown if the external web service returns an exception
	 */
	@GET
	@Path("/{id}")
	@Produces({ "application/json" })
	void getMovie(@PathParam("id") String id) throws InvalidInputException, MovieNotFoundException, ClientProtocolException, MovieApiException;
	

	@GET
	@Path("/now_playing")
	@Produces({ "application/json" })
	void getNowPlaying() throws ClientProtocolException, MovieApiException;
}
