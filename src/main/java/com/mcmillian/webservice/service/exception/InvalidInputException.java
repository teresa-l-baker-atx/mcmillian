package com.mcmillian.webservice.service.exception;

public class InvalidInputException extends MovieApiException {

	private static final long serialVersionUID = 1L;

	public InvalidInputException(String msg) {
		super(msg);
	}
}
