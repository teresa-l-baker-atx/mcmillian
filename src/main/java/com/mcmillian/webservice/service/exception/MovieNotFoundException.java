package com.mcmillian.webservice.service.exception;

public class MovieNotFoundException extends MovieApiException {

	private static final long serialVersionUID = 1L;

	public MovieNotFoundException(String msg) {
		super(msg);
	}
}