package com.mcmillian.webservice.service.exception;

public class MovieApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MovieApiException(String msg) {
		super(msg);
	}

}