package com.mcmillian.webservice.service.impl;

import java.io.StringReader;
import java.util.List;

import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PathParam;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.mcmillian.entity.MovieEntity;
import com.mcmillian.service.MovieManager;
import com.mcmillian.webservice.service.MoviesApi;
import com.mcmillian.webservice.service.converter.MovieConverter;
import com.mcmillian.webservice.service.exception.InvalidInputException;
import com.mcmillian.webservice.service.exception.MovieApiException;
import com.mcmillian.webservice.service.exception.MovieNotFoundException;

/**
 * Should document this class...
 */
public class MoviesApiImpl implements MoviesApi {

	private static final String MIME_TYPE_JSON = "application/json";
	private static final String API_KEY = "411dc5633f23ea1ff073371944b51136";	// should be from a configuration...
	private static final String MOVIES_URL = "https://api.themoviedb.org/3/movie/";	

	
	@Autowired
	private MovieConverter movieConverter;
	
	@Autowired
	private MovieManager movieManager;
		
	
	@Override
	public void getMovie(@PathParam("id") String id) throws InvalidInputException, MovieNotFoundException, ClientProtocolException, MovieApiException {
		if(null == id || 0 == id.trim().length()) {
			throw new InvalidInputException("id is missing");
		}
		
		String url = MOVIES_URL + id +"?api_key=" + API_KEY;
		String responseText= null;
		try {
			responseText = getHTTPResponse(url , "", HttpMethod.GET.toString());
		} catch( ClientProtocolException e) {
			throw e;
		} catch (Exception e) {
			throw new MovieNotFoundException(e.getLocalizedMessage());
		}
		
		if(null == responseText) {
			throw new MovieNotFoundException("no response");
		}
		
		// check for errors
		JsonNumber errorJsonNum = convertToJson(responseText).getJsonNumber("status_code"); 
		// Retrieve localized error message for specific JSON errors
		if (null != errorJsonNum) {
			String errorMessage = getErrorMessage(errorJsonNum);
			
			// this is a horrible design but don't have time to clean it up...
			if (34 == errorJsonNum.intValue()) {
				throw new MovieNotFoundException(errorMessage);
			}
			throw new MovieApiException(errorMessage);
		}

		// convert to Movie
		MovieEntity movie = movieConverter.convertToMovie(responseText);
		
		try {
			if (null == movieManager.findMovieById(movie.getMovieId())) {
				movieManager.addMovie(movie);
			}
		} catch (Exception e) {
			// log error
			error(e.getLocalizedMessage());
		}

		return;	
	}
	

	@Override
	public void getNowPlaying() throws ClientProtocolException, MovieApiException {
		String url = MOVIES_URL + "now_playing?api_key=" + API_KEY;
		String responseText= null;
		try {
			responseText = getHTTPResponse(url , "", HttpMethod.GET.toString());
		} catch( ClientProtocolException e) {
			throw e;
		} catch (Exception e) {
			throw new MovieApiException(e.getLocalizedMessage());
		}
		
		if(null == responseText) {
			throw new MovieApiException("no response");
		}
		
		// check for errors
		JsonNumber errorJsonNum = convertToJson(responseText).getJsonNumber("status_code"); 
		// Retrieve localized error message for specific JSON errors
		if (null != errorJsonNum) {
			String errorMessage = getErrorMessage(errorJsonNum);
			throw new MovieApiException(errorMessage);
		}

		// convert to Movie list
		List<MovieEntity> playingList = movieConverter.convertToMovieList(responseText);
		
		// Persist to back end database
		for(MovieEntity movie : playingList) {
			try {
				if(null == movieManager.findMovieById(movie.getMovieId())) {
					movieManager.addMovie(movie);
				}
			} catch (Exception e) {	
				// log error and continue so all other movies persisted
				error(e.getLocalizedMessage());
			}
		}

		return;
	}
	
	/**
	 * @param errorJson
	 * @return String
	 * 
	 *         Returns localized error message for given JSON error
	 */
	private String getErrorMessage(JsonNumber errorJsonNum) {
		String errorMessage = null;

		switch (errorJsonNum.intValue()) {
		case 7:
			errorMessage = "Invalid API key: You must be granted a valid key."; // should be configured and localized...
			break;
		case 34:
			errorMessage = "The resource you requested could not be found."; // should be configured and localized...
			break;
		}
		// more case statements...
		return errorMessage;
	}

	
	//
	// Helper methods
	//
	
	/**
	 * Really should be made more generic such that I simply pass an HttpMethod
	 * in, create a RestTemplate and call exchange but couldn't find the maven
	 * artifacts I was looking for...
	 * 
	 * @throws Exception - I would also prefer more specific exceptions but keeping it broad for demonstration sake
	 */
	private String getHTTPResponse(String url, String payloadData, String methodType) throws Exception { 

		log("Method Type :: "+ methodType);

		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		
		HttpRequestBase httpCall;
	
	
		if(methodType.equalsIgnoreCase(HttpMethod.GET.toString())) {

			httpCall = new HttpGet(url);
			httpCall.addHeader("Accept", MIME_TYPE_JSON);

			try {

				response = httpclient.execute(httpCall);	// IOException, ClientProtocolException

				HttpEntity entity = response.getEntity(); 
				
				String responseStr = EntityUtils.toString(entity);
				log(methodType+ " call for "+ url+" response received :: "+responseStr);
				return responseStr;

			} catch( ClientProtocolException e) {

				error("callget exception : : "+e.getMessage());
				throw e;
			}  catch( Exception e){

				error("callget exception : : "+e.getMessage());
				throw e;
			}

//		} else if(methodType.equalsIgnoreCase("put")) {

		}
		
		return null;
	}
	
	
	private static JsonObject convertToJson(String stringToConvert) {
		JsonReader reader = Json.createReader(new StringReader(stringToConvert));
		JsonObject object = reader.readObject();
		reader.close();

		return object;
	}


	private void log(String string) {
		System.out.println("INFO: " + string);
		
	}
		
		
	private void error(String string) {
		System.out.println("ERROR: " + string);
		
	}

}
