package com.mcmillian.webservice.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;


// A movie entry should contain at a minimum the following information:

//name
//
//genre
//
//year released
//
//rating
//
//list of spoken languages
	
//	JSON record
//  "vote_count": 1312,
//  "id": 429617,
//  "video": false,
//  "vote_average": 7.8,
//  "title": "Spider-Man: Far from Home",
//  "popularity": 496.88,
//  "poster_path": "/rjbNpRMoVvqHmhmksbokcyCr7wn.jpg",
//  "original_language": "en",
//  "original_title": "Spider-Man: Far from Home",
//  "genre_ids": [
//    28,
//    12,
//    878
//  ],
//  "backdrop_path": "/dihW2yTsvQlust7mSuAqJDtqW7k.jpg",
//  "adult": false,
//  "overview": "Peter Parker and his friends go on a summer trip to Europe. However, they will hardly be able to rest - Peter will have to agree to help Nick Fury uncover the mystery of creatures that cause natural disasters and destruction throughout the continent.",
//  "release_date": "2019-06-28"

public class Movie {
	

	private Long id;
	private String name;
	private List<Genre> genres;		
	private String releaseYear;
	private List<String> languages;		// LANGUAGE really should have been it's own object like GENRE...
	private BigDecimal rating;			// hopefully this lines up with "popularity"
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Genre> getGenres() {
		return genres;
	}
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public BigDecimal getRating() {
		return rating;
	}
	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null || getClass() != other.getClass()) {
			return false;
		}
		Movie movie = (Movie) other;
		return Objects.equals(this.id, movie.id) && 
				Objects.equals(this.name, movie.name) && 
				Objects.equals(this.genres, movie.genres) && 
				Objects.equals(this.releaseYear, movie.releaseYear) && 
				Objects.equals(this.languages, movie.languages) && 
				Objects.equals(this.rating, movie.rating);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Movie {\n");
		sb.append("    id: ").append(id).append("\n");
		sb.append("    name: ").append(name).append("\n");
		sb.append("    genres: ").append(genres).append("\n");
		sb.append("    releaseYear: ").append(releaseYear).append("\n");
		sb.append("    languages: ").append(languages).append("\n");
		sb.append("    rating: ").append(rating).append("\n");
		sb.append("}");
		return sb.toString();
	}

}
