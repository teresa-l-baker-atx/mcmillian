package com.mcmillian.webservice.model;

import java.util.Objects;

/**
 * A genre returned from the web service
 * 
 * id,
 * name
 * 
 * https://api.themoviedb.org/3/genre/movie/list
 *
 */
public class Genre {

	private Long id;
	private String name;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (other == null || getClass() != other.getClass()) {
			return false;
		}
		Genre genre = (Genre) other;
		return Objects.equals(this.id, genre.id) && Objects.equals(this.name, genre.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Genre {\n");
		sb.append("    id: ").append(id).append("\n");
		sb.append("    name: ").append(name).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
