package com.mcmillian.service;

import java.util.List;

import com.mcmillian.entity.GenreEntity;

public interface GenreManager {
	
	public void addGenre(GenreEntity genre);
    public List<GenreEntity> getAllGenres();
}
