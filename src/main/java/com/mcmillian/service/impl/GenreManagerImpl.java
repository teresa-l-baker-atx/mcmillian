package com.mcmillian.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mcmillian.dao.GenreDao;
import com.mcmillian.entity.GenreEntity;
import com.mcmillian.service.GenreManager;

@Service
public class GenreManagerImpl implements GenreManager {

	 @Autowired
	 private GenreDao genreDao;
	 
	@Override
	@Transactional
	public void addGenre(GenreEntity genre) {
		genreDao.addGenre(genre);
	}

	@Override
	@Transactional
	public List<GenreEntity> getAllGenres() {
		return genreDao.getAllGenres();
	}

	public void setGenreDao(GenreDao genreDao) {
		this.genreDao = genreDao;
	}

}
