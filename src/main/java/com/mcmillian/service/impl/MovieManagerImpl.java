package com.mcmillian.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mcmillian.dao.MovieDao;
import com.mcmillian.entity.MovieEntity;
import com.mcmillian.service.MovieManager;

@Service
public class MovieManagerImpl implements MovieManager {

	@Autowired
	private MovieDao movieDao;

	@Override
	@Transactional
	public void addMovie(MovieEntity movie) {
		movieDao.addMovie(movie);
	}

	@Override
	@Transactional
	public List<MovieEntity> getAllMovies() {
		return movieDao.getAllMovies();
	}

	@Override
	@Transactional
	public MovieEntity findMovieById(long id) {
		return movieDao.findMovieById(id);
	}

	public void setMovieDao(MovieDao movieDao) {
		this.movieDao = movieDao;
	}
}
