package com.mcmillian.service;

import java.util.List;

import com.mcmillian.entity.MovieEntity;

public interface MovieManager {

	public void addMovie(MovieEntity movie);
    public List<MovieEntity> getAllMovies();
    public MovieEntity findMovieById(long id);
}
