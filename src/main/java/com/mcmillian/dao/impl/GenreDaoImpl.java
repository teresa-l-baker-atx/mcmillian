package com.mcmillian.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mcmillian.dao.GenreDao;
import com.mcmillian.entity.GenreEntity;

@Repository
public class GenreDaoImpl implements GenreDao {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public void addGenre(GenreEntity genre) {
		if(null == genre) {
			throw new NullPointerException("genre is null");
		}
 		this.sessionFactory.getCurrentSession().save(genre);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GenreEntity> getAllGenres() {
		return this.sessionFactory.getCurrentSession().createQuery("from GenreEntity").list();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
