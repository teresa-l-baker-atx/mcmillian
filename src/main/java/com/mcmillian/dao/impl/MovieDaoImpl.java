package com.mcmillian.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mcmillian.dao.MovieDao;
import com.mcmillian.entity.MovieEntity;

@Repository
public class MovieDaoImpl implements MovieDao {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public void addMovie(MovieEntity movie) {
		if(null == movie) {
			throw new NullPointerException("movie is null");
		}
		this.sessionFactory.getCurrentSession().save(movie);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieEntity> getAllMovies() {
		return this.sessionFactory.getCurrentSession().createQuery("from MovieEntity").list();
	}

	@Override
	public MovieEntity findMovieById(long id) {
		if(id < 0) {
			throw new IllegalArgumentException("movie id is not a positive number");
		}
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieEntity.class)
				.add(Restrictions.eq("movieId", id));
		return (MovieEntity) criteria.uniqueResult();
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
