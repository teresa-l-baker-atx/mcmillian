package com.mcmillian.dao;

import java.util.List;

import com.mcmillian.entity.GenreEntity;

public interface GenreDao {
	
	public void addGenre(GenreEntity genre);
	
    public List<GenreEntity> getAllGenres();

}
