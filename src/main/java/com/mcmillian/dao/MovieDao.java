package com.mcmillian.dao;

import java.util.List;

import com.mcmillian.entity.MovieEntity;



public interface MovieDao {
	
	public void addMovie(MovieEntity movie);
	
    public List<MovieEntity> getAllMovies();
    
    public MovieEntity findMovieById(long id);
}
