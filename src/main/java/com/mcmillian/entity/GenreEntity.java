package com.mcmillian.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class GenreEntity {

	@Id
    @Column(name="ID")
    @GeneratedValue
	private long primaryKey;
	
	@Column(name = "MOVIE_ID", unique = true, nullable = false)
	private Long movieId;
	
	@Column(name = "NAME", unique = false, nullable = false, length = 2000)
	private String name;
	
	
	public long getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}
	
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GenreEntity)) {
			return false;
		}
		final GenreEntity that = (GenreEntity) other;
		return (getPrimaryKey() == that.getPrimaryKey());
	}

	@Override
	public int hashCode() {
		return Long.valueOf(getPrimaryKey()).hashCode();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class GenreEntity {\n");
		sb.append("    movieId: ").append(movieId).append("\n");
		sb.append("    name: ").append(name).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
