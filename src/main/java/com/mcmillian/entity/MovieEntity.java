package com.mcmillian.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.mcmillian.webservice.model.Genre;

public class MovieEntity {
	@Id
    @Column(name="ID")
    @GeneratedValue
	private long primaryKey;
	
	@Column(name = "MOVIE_ID", unique = true, nullable = false)
	private Long movieId;
	
	@Column(name = "NAME", unique = false, nullable = false, length = 2000)
	private String name;

	@Column(name = "GENRE")
	private List<Genre> genres;		

	@Column(name = "RELEASE_YEAR", unique = false, nullable = false, length = 4)
	private String releaseYear;

	@Column(name = "LANGUAGES")
	private List<String> languages;	

	@Column(name = "RATING", nullable = false)
	private BigDecimal rating;
	
	
	public long getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Genre> getGenres() {
		return genres;
	}
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public BigDecimal getRating() {
		return rating;
	}
	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MovieEntity)) {
			return false;
		}
		final MovieEntity that = (MovieEntity) other;
		return (getPrimaryKey() == that.getPrimaryKey());
	}

	@Override
	public int hashCode() {
		return Long.valueOf(getPrimaryKey()).hashCode();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MovieEntity {\n");
		sb.append("    movieId: ").append(movieId).append("\n");
		sb.append("    name: ").append(name).append("\n");
		sb.append("    genres: ").append(genres).append("\n");
		sb.append("    releaseYear: ").append(releaseYear).append("\n");
		sb.append("    languages: ").append(languages).append("\n");
		sb.append("    rating: ").append(rating).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
