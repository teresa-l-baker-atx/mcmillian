
McMilliam coding challenge:  https://bitbucket.org/teresa-l-baker-atx/mcmillian/

There are many things that are not complete on this project. I will list some of the most troubling to me below.

1) I could not get H2 running on my laptop because there is a system process using port 8082 and
   I could not determine what service was using the port. Therefore I do not know if I have hibernate 
   set up correctly.
   
2) Tests - I wish I had more time to do some mocking rather than just return AssertTrue(true).
           I also know that I should have written tests for the GenreManager and MovieManger objects.
		   This would have included transaction testing.
		   
3) I took a short cut on the MovieConverter which should read the json and create the Entity objects. 
   (I do realize that in this use I do not need the Movie and Genre model objects but left them in.)

4) I realize that both Genre and Language should be a many to one relationship with a Movie. I believe
   that Language should have been it's own object like Genre but took a shortcut and use a List of String.
   This ties into the fact that I could not get H2 running so I took my best guess and mapped the two 
   fields to ARRAY. The reality is that the way I have it modeled I do not believe Genre correct in Movie.

5) I don't like the way I have getHTTPResponse coded in MoviesApiImpl.
   It should be made more generic such that I simply pass an HttpMethod in, create a RestTemplate 
   and call exchange but couldn't find the maven artifacts I was looking for.

6) I need to set up real logging rather than use System.out
   
7) I didn't clean up my maven dependencies and I know I have extras in there.

8) I would have loved to write some integration tests in order to execute the rest service.

This was a very intriguing challenge and I really enjoyed it!